package nanda.fikri.app06

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var fragMatkul : FragmentMataKuliah
    lateinit var fragNilai : FragmentNilai

    lateinit var ft : FragmentTransaction
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragMatkul = FragmentMataKuliah()
        fragNilai = FragmentNilai()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragProdi).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragMhs).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,225,255,255))
                FrameLayout.visibility = View.VISIBLE
            }

            R.id.itemAbout -> FrameLayout.visibility = View.GONE
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemMatkul ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragMatkul).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                FrameLayout.visibility = View.VISIBLE
                return true
            }

            R.id.itemNilai ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragNilai).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,255,255,225))
                FrameLayout.visibility = View.VISIBLE
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
