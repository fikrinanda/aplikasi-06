package nanda.fikri.app06

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(contet : Context):SQLiteOpenHelper(contet, DB_Name,null, DB_VER) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs = "create table mhs(nim text primary key, nama text not null, id_prodi int not null)"
        val tProdi = "create table prodi(id_prodi integer primary key autoincrement, nama_prodi text not null)"
        val insProdi = "insert into prodi(nama_prodi) values ('MI'),('AK'),('ME')"
        val tMatkul = "create table matkul(id_matkul text primary key, nama_matkul text not null, id_prodi int not null)"
        val tNilai = "create table nilai(id_nilai integer primary key autoincrement,nama text not null, nilai text not null)"
        db?.execSQL(tMhs)
        db?.execSQL(tProdi)
        db?.execSQL(insProdi)
        db?.execSQL(tMatkul)
        db?.execSQL(tNilai)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object{
        val DB_Name = "mahasiswa"
        val DB_VER = 1
    }
}